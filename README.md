# Example C++ application using OpenSSL

## Installing dependencies

### Debian based distributions

`sudo apt-get install libssl-dev`

### Arch Linux based distributions

`sudo pacman -S openssl`

### OS X

`brew install openssl`

## Compiling and running

### From terminal

```sh
git clone https://gitlab.com/ntnu-tdat3020/openssl-example
cd openssl-example
mkdir build
cd build
cmake ..
make
./example
```

### Using [juCi++](https://gitlab.com/cppit/jucipp)

```sh
git clone https://gitlab.com/ntnu-tdat3020/openssl-example
juci openssl-example
```

Choose Compile and Run in the Project menu.

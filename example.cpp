#include <iomanip>
#include <iostream>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <sstream>
#include <string>

/// Return hex string from bytes in input string.
std::string hex(const std::string &input) {
  std::stringstream hex_stream;
  hex_stream << std::hex << std::internal << std::setfill('0');
  for (auto &byte : input)
    hex_stream << std::setw(2) << (int)(unsigned char)byte;
  return hex_stream.str();
}

/// Return the SHA-512 (512-bit) hash from input.
std::string sha512(const std::string &input) noexcept {
  auto evp_md = EVP_sha512();

  std::string hash(SHA512_DIGEST_LENGTH, '\0');

  auto ctx = EVP_MD_CTX_create();
  EVP_MD_CTX_init(ctx);
  EVP_DigestInit_ex(ctx, evp_md, nullptr);
  EVP_DigestUpdate(ctx, input.data(), input.size());
  EVP_DigestFinal_ex(ctx, reinterpret_cast<unsigned char *>(&hash[0]), nullptr);
  EVP_MD_CTX_destroy(ctx);

  return hash;
}

int main() {
  std::cout << hex(sha512("Password123")) << std::endl;
}
